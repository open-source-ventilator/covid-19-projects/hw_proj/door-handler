# Disclaimer

*The plans, documents and other materials (“Material”) contained on this website are intended to facilitate the design of a prototype reproducible medical device to be used if required during the Covid-19 pandemic. The Material is not itself a medical device. The Material has not been tested and has not been approved for use in humans or animals by any regulatory authority of any country.*

*By using the Material, you are agreeing to the following disclaimer.*

*OpenLung offers the Material as-is and as-available, and makes no representations or warranties of any kind whatever concerning the Material, whether express, implied, statutory, or other. This includes, without limitation, warranties of merchantability, fitness for a particular purpose, non-infringement, absence of latent or other defects, accuracy, or the presence or absence of errors, whether or not known or discoverable.*

*To the extent possible, in no event will OpenLung be liable to you on any legal theory (including, without limitation, negligence) or otherwise for any direct, special, indirect, incidental, consequential, punitive, exemplary, or other losses, costs, expenses, or damages arising out of the Material or use of the Material, even if OpenLung / Open Source Ventilator has been advised of the possibility of such losses, costs, expenses, or damages.*

*The disclaimer of warranties and limitation of liability provided above shall be interpreted in a manner that, to the extent possible, most closely approximates an absolute disclaimer and waiver of all liability.*

# Door Handler

An open source design for a door handle gripping key-chain that could help in the spread of surface germs or viruses, inspired by the COVID-19 pandemic.

![Door Handler](images/Door_Handler.png)

# 3D Printable STL

### View and Download the STL file [here](models/OpenLung_Door_Handler.STL).

- Note: We recommend a material like NinjaFlex to allow for flexibility.